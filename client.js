var species = namespace('World.Species');

species.Human = Class({	
	constructor : function(name){
		this.name = name;
	},
	helloWorld : function(){
		console.log(this.name + " says Hello world");
	},
	whoIsYourDaddy : function(){
		console.log("None");
	},
	getName : function(){
		return this.name;
	}
}).create();

var human = new species.Human("human");
human.helloWorld();
human.whoIsYourDaddy();

species.Man = Class({
	constructor : function(name, height){
		this.base(name);
		this.height = height;
	},	
	tellHeight : function(){
		console.log(this.name + " is "  + this.height + " feet tall");
	},
	whoIsYourDaddy : function(){
		console.log("Human");
	}
}).extend(species.Human).create();

var man = new species.Man("Anshul", 6);
man.helloWorld();
man.whoIsYourDaddy();
man.tellHeight();

species.Pretty = Interface({
	isShePretty : function(anotherPretty){}
});

species.Woman = Class({
	constructor : function(name){
		this.base(name);
	},	
	isShePretty : function(anotherPretty){
		console.log("No "+ anotherPretty.getName() +" is not pretty." + this.name + " is pretty.")
	}	
}).extend(species.Human).implement(species.Pretty).create();

var woman = new species.Woman("Shakira");
woman.isShePretty(new species.Woman("Britney"));